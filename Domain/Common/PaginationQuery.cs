﻿namespace Domain.Common
{
    public class PaginationQuery
    {
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        
        public PaginationQuery()
        {
            PageNumber = null;
            PageSize = null;
        }
        
        public PaginationQuery(int pageNumber, int pageSize)
        {
            PageNumber = pageNumber;
            // Restrict max page size
            PageSize = pageSize > 6 ? 6 : pageSize;
        }
    }
}
