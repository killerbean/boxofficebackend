﻿using System.Collections.Generic;

namespace Domain.Common
{
    public class PagedResponse<T>
    {
        public IEnumerable<T> Lists { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? NextPageNumber { get; set; }
        public int? PreviousPageNumber { get; set; }

        public PagedResponse(IEnumerable<T> lists)
        {
            Lists = lists;
        }
    }
}
