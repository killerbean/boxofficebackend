﻿using System;
using LinqToDB.Mapping;

namespace Domain.Entities
{
    [Table(Schema = "dbo", Name = "TBL_F4NTEC_AGENDA")]
    public class TheatreAgenda
    {
        [Column("id"),PrimaryKey, Identity] public int Id { get; set; }
        [Column("show_id"),       Nullable] public int? ShowId { get; set; }
        [Column("day_id"),   Nullable] public int? DayId { get; set; }
        [Column("time_id"),   Nullable] public int? TimeId { get; set; }
        [Column("agenda_date"),   Nullable] public DateTime? AgendaDate { get; set; }
        [Column("tickets_qty"),   Nullable] public int? TicketsQty { get; set; }
        

        [Association (ThisKey = "ShowId", OtherKey = "Id")]
        public TheatreShow TheatreShow { get; set; }
        
        [Association (ThisKey = "DayId", OtherKey = "Id")]
        public DayOfTheWeek Day { get; set; }
        
        [Association (ThisKey = "TimeId", OtherKey = "Id")]
        public TimeOfTheDay TimeFrame { get; set; }
    }
}
