﻿using LinqToDB.Mapping;

namespace Domain.Entities
{
    [Table(Schema = "dbo", Name = "TBL_F4NTEC_TIME_OF_THE_DAY")]
    public class TimeOfTheDay
    {
        [Column("id"),PrimaryKey, Identity] public int Id { get; set; }
        [Column("name"),           NotNull] public string Name { get; set; }
    }
}