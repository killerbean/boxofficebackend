﻿using LinqToDB.Mapping;

namespace Domain.Entities
{
    [Table(Schema = "dbo", Name = "TBL_F4NTEC_SHOW")]
    public class TheatreShow : AuditableEntity
    {
        [Column("id"),PrimaryKey, Identity] public int Id { get; set; }
        [Column("name"),           NotNull] public string Name { get; set; }
        [Column("poster_url"),     NotNull] public string PosterUrl { get; set; }
    }
}
