﻿using LinqToDB.Mapping;

namespace Domain.Entities
{
    [Table(Schema = "dbo", Name = "TBL_F4NTEC_DAY_OF_WEEK")]
    public class DayOfTheWeek
    {
        [Column("id"),PrimaryKey, Identity] public int Id { get; set; }
        [Column("name"),           NotNull] public string Name { get; set; }
    }
}
