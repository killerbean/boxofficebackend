﻿using LinqToDB.Mapping;

namespace Domain.Entities
{
    [Table(Schema = "dbo", Name = "TBL_F4NTEC_USER")]
    public class User : AuditableEntity
    {
        [Column("id"),PrimaryKey, Identity] public int Id { get; set; }
        [Column("email"),          NotNull] public string Email { get; set; }
        [Column("password"),      Nullable] public string Password { get; set; }
        [Column("is_registered"),  NotNull] public bool IsRegistered { get; set; }
        [Column("is_admin"),       NotNull] public bool IsAdmin { get; set; }
    }
}
