﻿using System;
using LinqToDB.Mapping;

namespace Domain.Entities
{
    public class AuditableEntity
    {
        [Column("created_by"),        NotNull] public string CreatedBy { get; set; }
        [Column("created"),           NotNull] public DateTime Created { get; set; }
        [Column("last_modified_by"),    Nullable] public string? LastModifiedBy { get; set; }
        [Column("last_modified"), Nullable] public DateTime? LastModified { get; set; }
    }
}
