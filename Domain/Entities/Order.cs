﻿using LinqToDB.Mapping;

namespace Domain.Entities
{
    [Table(Schema = "dbo", Name = "TBL_F4NTEC_ORDER")]
    public class Order
    {
        [Column("id"),PrimaryKey,    Identity] public int Id { get; set; }
        [Column("user_id"),          Nullable] public int? UserId { get; set; }
        [Column("agenda_id"),          Nullable] public int? AgendaId { get; set; }
        [Column("tickets_ordered"), Nullable] public int? TicketsOrdered { get; set; }
        
        public User User { get; set; }
        public TheatreAgenda TheatreAgenda { get; set; }
    }
}