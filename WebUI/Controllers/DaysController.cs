﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Application.Day.Queries.GetDays;

namespace BoxOffice.Controllers
{
    public class DaysController : ApiController
    {
        [HttpGet]
        public async Task<ActionResult<DayOfTheWeekVm>> Get()
        {
            return await Mediator.Send(new GetDaysOfWeekQuery());
        }
    }
}
