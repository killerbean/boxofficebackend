﻿using AutoMapper;
using Domain.Common;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Application.Common.Behaviours;
using Application.Agenda.Queries.GetAgenda;
using Application.Agenda.Queries.GetAgendas;
using Application.Agenda.Commands.SendEmail;
using Application.Agenda.Commands.CreateAgenda;
using Application.Agenda.Commands.DeleteAgenda;
using Application.Agenda.Commands.UpdateAgenda;

namespace BoxOffice.Controllers
{
    public class AgendasController : ApiController
    {
        private readonly IMapper _mapper;

        public AgendasController(IMapper mapper)
        {
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<AgendaVm>> Get([FromQuery] PaginationQuery paginationQuery)
        {
            var paginationFilter = _mapper.Map<PaginationFilter>(paginationQuery);
            var response = await Mediator.Send(new GetAgendasQuery { Filter = paginationFilter });
            var paginationResponse = new PagedResponse<AgendaDto>(response.Lists);

            if (paginationFilter == null || paginationFilter.PageNumber < 1 || paginationFilter.PageSize < 1)
            {
                Ok(paginationResponse);
            }

            var modifiedResponse = PaginationHelpers.CreatePaginationResponse(paginationFilter, paginationResponse);
            return Ok(modifiedResponse);
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<AgendaVm>> GetById(long id)
        {
            return await Mediator.Send(new GetAgendaQuery { Id = id });
        }
        
        [HttpPost]
        public async Task<ActionResult<long>> Create(CreateAgendaCommand command)
        {
            return await Mediator.Send(command);
        }
        
        [HttpPut("{id}")]
        public async Task<ActionResult> Update(long id, UpdateAgendaCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }
        
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteAgendaCommand { Id = id });

            return NoContent();
        }
        
        [HttpPost("send-email")]
        public async Task<ActionResult> SendEmail(SendEmailCommand command)
        {
             await Mediator.Send(command);
             return NoContent();
        }
    }
}
