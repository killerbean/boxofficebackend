﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Application.Show.Queries.GetShow;
using Application.Show.Queries.GetShows;
using Application.Show.Commands.CreateShow;
using Application.Show.Commands.DeleteShow;
using Application.Show.Commands.UpdateShow;

namespace BoxOffice.Controllers
{
    public class ShowsController : ApiController
    {
        [HttpGet]
        public async Task<ActionResult<ShowVm>> Get()
        {
            return await Mediator.Send(new GetShowsQuery());
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<ShowVm>> GetById(long id)
        {
            return await Mediator.Send(new GetShowQuery { Id = id });
        }
        
        [HttpPost]
        public async Task<ActionResult<long>> Create(CreateShowCommand command)
        {
            return await Mediator.Send(command);
        }
        
        [HttpPut("{id}")]
        public async Task<ActionResult> Update(long id, UpdateShowCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }
        
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteShowCommand { Id = id });

            return NoContent();
        }
    }
}
