﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Application.Time.Queries.GetTimes;

namespace BoxOffice.Controllers
{
    public class TimesController : ApiController
    {
        [HttpGet]
        public async Task<ActionResult<TimeOfTheDayVm>> Get()
        {
            return await Mediator.Send(new GetTimesOfDayQuery());
        }
    }
}
