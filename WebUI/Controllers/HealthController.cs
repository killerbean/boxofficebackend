﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace BoxOffice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]    
    public class HealthController : ApiController
    {
        [AllowAnonymous]
        [HttpGet]
        // TODO: Basic healthcheck approach, can be replaced with something like this
        // https://scottsauber.com/2017/05/22/using-the-microsoft-aspnetcore-healthchecks-package/
        public ActionResult Get() => Ok( new {status = "up"});
    }
}
