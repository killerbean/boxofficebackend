﻿using MediatR;
using LinqToDB;
using System.Linq;
using Domain.Entities;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Application.Show.Queries.GetShows;

namespace Application.Show.Commands.CreateShow
{
    public class CreateShowCommand : IRequest<int>
    {
        public ShowDto Show { get; set; }
        
        public class CreateShowCommandHandler : IRequestHandler<CreateShowCommand, int>
        {
            private readonly IDateTime _dateTime;
            private readonly IApplicationDbContext _db;
            private readonly ICurrentUserService _currentUserService;
            
            public CreateShowCommandHandler(IApplicationDbContext db, IDateTime dateTime, ICurrentUserService currentUserService
            )
            {
                _db = db;
                _dateTime = dateTime;
                _currentUserService = currentUserService;
            }
            
            public async Task<int> Handle(CreateShowCommand request, CancellationToken cancellationToken)
            {
                if (request.Show == null)
                {
                    return 0;
                }

                await _db.TheatreShows
                    .InsertAsync(() => new TheatreShow
                    {
                        Name = request.Show.Name,
                        PosterUrl = request.Show.PosterUrl,
                        Created = _dateTime.Now,
                        CreatedBy = _currentUserService.UserId ?? "unregistered"
                    }, cancellationToken);

                var id = _db.TheatreShows.FirstOrDefault(ts => ts.Name == request.Show.Name && 
                                                               ts.PosterUrl == request.Show.PosterUrl)?.Id;

                return id ?? 0;
            }
        }
    }
}
