﻿using MediatR;
using LinqToDB;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;

namespace Application.Show.Commands.DeleteShow
{
    public class DeleteShowCommand : IRequest
    {
        public int Id { get; set; }
        
        public class DeleteShowCommandHandler : IRequestHandler<DeleteShowCommand>
        {
            private readonly IApplicationDbContext _db;
            public DeleteShowCommandHandler(IApplicationDbContext db)
            {
                _db = db;
            }
            
            public async Task<Unit> Handle(DeleteShowCommand request, CancellationToken cancellationToken)
            {
                var entity = await _db.TheatreShows
                    .FirstOrDefaultAsync(ts => ts.Id == request.Id, cancellationToken);

                if (entity == null)
                {
                    return Unit.Value;
                }

                await _db.TheatreShows
                    .Where(ts => ts.Id == request.Id)
                    .DeleteAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}
