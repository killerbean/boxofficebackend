﻿using MediatR;
using LinqToDB;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;

namespace Application.Show.Commands.UpdateShow
{
    public class UpdateShowCommand : IRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PosterUrl { get; set; }
        
        public class UpdateShowCommandHandler : IRequestHandler<UpdateShowCommand>
        {
            private readonly IDateTime _dateTime;
            private readonly IApplicationDbContext _db;
            private readonly ICurrentUserService _currentUserService;
            
            public UpdateShowCommandHandler(IApplicationDbContext db, IDateTime dateTime, ICurrentUserService currentUserService)
            {
                _db = db;
                _dateTime = dateTime;
                _currentUserService = currentUserService;
            }
            
            public async Task<Unit> Handle(UpdateShowCommand request, CancellationToken cancellationToken)
            {
                var entity = await _db.TheatreShows
                    .FirstOrDefaultAsync(ts => ts.Id == request.Id, cancellationToken);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(TheatreShow), request.Id);
                }

                await _db.TheatreShows
                    .Where(ts => ts.Id == request.Id)
                    .Set(ts => ts.Name, request.Name)
                    .Set(ts => ts.PosterUrl, request.PosterUrl)
                    .Set(ts => ts.LastModifiedBy, _currentUserService.UserId ?? "unregistered")
                    .Set(ts => ts.LastModified, _dateTime.Now)
                    .UpdateAsync(cancellationToken);
                
                return Unit.Value;
            }
        }
    }
}
