﻿using System.Collections.Generic;

namespace Application.Show.Queries.GetShows
{
    public class ShowVm
    {
        public IList<ShowDto> Lists { get; set; }
    }
}
