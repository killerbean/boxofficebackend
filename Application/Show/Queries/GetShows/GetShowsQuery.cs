﻿using MediatR;
using LinqToDB;
using AutoMapper;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using AutoMapper.QueryableExtensions;

namespace Application.Show.Queries.GetShows
{
    public class GetShowsQuery : IRequest<ShowVm>
    {
        public class GetShowsQueryHandler : IRequestHandler<GetShowsQuery, ShowVm>
        {
            private readonly IApplicationDbContext _db;
            private readonly IMapper _mapper;
            
            public GetShowsQueryHandler(IApplicationDbContext db, IMapper mapper)
            {
                _db = db;
                _mapper = mapper;
            }
            
            public async Task<ShowVm> Handle(GetShowsQuery request, CancellationToken cancellationToken)
            {
                var vm = new ShowVm
                {
                    Lists = await _db.TheatreShows
                        .ProjectTo<ShowDto>(_mapper.ConfigurationProvider)
                        .ToListAsync(cancellationToken)
                };

                return vm;
            }
        }
    }
}
