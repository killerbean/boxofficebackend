﻿using System.Collections.Generic;
using Application.Common.Mappings;
using AutoMapper;
using Domain.Entities;

namespace Application.Show.Queries.GetShows
{
    public class ShowDto : IMapFrom<TheatreShow>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PosterUrl { get; set; }
        
        public void Mapping(Profile profile)
        {
            profile.CreateMap(typeof(TheatreShow), GetType());
        }
    }
}
