﻿using MediatR;
using LinqToDB;
using AutoMapper;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using AutoMapper.QueryableExtensions;
using Application.Show.Queries.GetShows;

namespace Application.Show.Queries.GetShow
{
    public class GetShowQuery : IRequest<ShowVm>
    {
        public long Id { get; set; }

        public class GetShowQueryHandler : IRequestHandler<GetShowQuery, ShowVm>
        {
            private readonly IApplicationDbContext _db;
            private readonly IMapper _mapper;
            
            public GetShowQueryHandler(IApplicationDbContext db, IMapper mapper)
            {
                _db = db;
                _mapper = mapper;
            }
            
            public async Task<ShowVm> Handle(GetShowQuery request, CancellationToken cancellationToken)
            {
                var vm = new ShowVm
                {
                    Lists = await _db.TheatreShows
                        .ProjectTo<ShowDto>(_mapper.ConfigurationProvider)
                        .Where(ts => ts.Id == request.Id)
                        .ToListAsync(cancellationToken)
                };

                return vm;
            }
        }
    }
}