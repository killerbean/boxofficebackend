﻿using MediatR;
using LinqToDB;
using AutoMapper;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using AutoMapper.QueryableExtensions;

namespace Application.Time.Queries.GetTimes
{
    public class GetTimesOfDayQuery : IRequest<TimeOfTheDayVm>
    {
        public class GetAgendasQueryHandler : IRequestHandler<GetTimesOfDayQuery, TimeOfTheDayVm>
        {
            private readonly IApplicationDbContext _db;
            private readonly IMapper _mapper;
            
            public GetAgendasQueryHandler(IApplicationDbContext db, IMapper mapper)
            {
                _db = db;
                _mapper = mapper;
            }
            
            public async Task<TimeOfTheDayVm> Handle(GetTimesOfDayQuery request, CancellationToken cancellationToken)
            {
                var vm = new TimeOfTheDayVm()
                {
                    Lists = await _db.TimesOfTheDay
                        .ProjectTo<TimeOfTheDayDto>(_mapper.ConfigurationProvider)
                        .ToListAsync(cancellationToken)
                };

                return vm;
            }
        }
    }
}
