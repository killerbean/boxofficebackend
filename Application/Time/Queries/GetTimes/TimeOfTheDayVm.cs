﻿using System.Collections.Generic;

namespace Application.Time.Queries.GetTimes
{
    public class TimeOfTheDayVm
    {
        public IList<TimeOfTheDayDto> Lists { get; set; }
    }
}
