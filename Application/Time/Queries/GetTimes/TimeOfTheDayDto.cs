﻿using AutoMapper;
using Domain.Entities;
using Application.Common.Mappings;

namespace Application.Time.Queries.GetTimes
{
    public class TimeOfTheDayDto : IMapFrom<TimeOfTheDay>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public void Mapping(Profile profile)
        {
            profile.CreateMap(typeof(TimeOfTheDay), GetType());
        }
    }
}
