﻿using System;
using MediatR;
using LinqToDB;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Agenda.Queries.GetAgendas;
using Application.Common.Interfaces;
using FluentEmail.Core;

namespace Application.Agenda.Commands.SendEmail
{
    public class SendEmailCommand : IRequest
    {
        public AgendaDto Agenda { get; set; }
        public string Mail { get; set; }

        public class CreateShowCommandHandler : IRequestHandler<SendEmailCommand>
        {
            private readonly IApplicationDbContext _db;
            
            public CreateShowCommandHandler(IApplicationDbContext db)
            {
                _db = db;
            }
            
            public async Task<Unit> Handle(SendEmailCommand request, CancellationToken cancellationToken)
            {
                if (request.Agenda.TicketsQty == 0)
                {
                    throw new Exception("Билеты на данное шоу закончились");
                }

                // Subtract 1 from current tickets qty
                await _db.TheatreAgendas
                    .Where(ts => ts.Id == request.Agenda.Id)
                    .Set(ts => ts.TicketsQty, request.Agenda.TicketsQty - 1)
                    .UpdateAsync(cancellationToken);
                
                // TODO: manage with real SMTP server
//                Email.From("cute.theatre@box.office")
//                    .To(request.Mail, "Ваш любимый театр")
//                    .Subject("Поздравляем")
//                    .Body("Ваш билет успешно оформлен")
//                    .Send();

                return Unit.Value;
            }
        }
    }
}