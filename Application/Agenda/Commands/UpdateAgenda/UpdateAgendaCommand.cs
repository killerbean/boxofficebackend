﻿using System;
using MediatR;
using LinqToDB;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;

namespace Application.Agenda.Commands.UpdateAgenda
{
    public class UpdateAgendaCommand : IRequest
    {
        public int Id { get; set; }
        public int? ShowId { get; set; }
        public int? DayId { get; set; }
        public int? TimeId { get; set; }
        public DateTime? AgendaDate { get; set; }
        public int? TicketsQty { get; set; }
        
        public class UpdateShowCommandHandler : IRequestHandler<UpdateAgendaCommand>
        {
            private readonly IDateTime _dateTime;
            private readonly IApplicationDbContext _db;
            private readonly ICurrentUserService _currentUserService;
            
            public UpdateShowCommandHandler(IDateTime dateTime, IApplicationDbContext db, ICurrentUserService currentUserService)
            {
                _dateTime = dateTime;
                _db = db;
                _currentUserService = currentUserService;
            }
            
            public async Task<Unit> Handle(UpdateAgendaCommand request, CancellationToken cancellationToken)
            {
                var entity = await _db.TheatreAgendas
                    .FirstOrDefaultAsync(ts => ts.Id == request.Id, cancellationToken);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(TheatreAgenda), request.Id);
                }

                await _db.TheatreAgendas
                    .Where(ts => ts.Id == request.Id)
                    .Set(ts => ts.ShowId, request.ShowId)
                    .Set(ts => ts.DayId, request.DayId)
                    .Set(ts => ts.TimeId, request.TimeId)
                    .Set(ts => ts.AgendaDate, request.AgendaDate)
                    .Set(ts => ts.TicketsQty, request.TicketsQty)
                    .UpdateAsync(cancellationToken);
                
                return Unit.Value;
            }
        }
    }
}
