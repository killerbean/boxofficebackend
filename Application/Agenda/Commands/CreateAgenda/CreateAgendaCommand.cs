﻿using MediatR;
using LinqToDB;
using System.Linq;
using Domain.Entities;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Application.Agenda.Queries.GetAgendas;

namespace Application.Agenda.Commands.CreateAgenda
{
    public class CreateAgendaCommand : IRequest<int>
    {
        public AgendaDto Agenda { get; set; }
        
        public class CreateShowCommandHandler : IRequestHandler<CreateAgendaCommand, int>
        {
            private readonly IDateTime _dateTime;
            private readonly IApplicationDbContext _db;
            private readonly ICurrentUserService _currentUserService;
            public CreateShowCommandHandler(IDateTime dateTime, IApplicationDbContext db, ICurrentUserService currentUserService)
            {
                _dateTime = dateTime;
                _db = db;
                _currentUserService = currentUserService;
            }
            
            public async Task<int> Handle(CreateAgendaCommand request, CancellationToken cancellationToken)
            {
                await _db.TheatreAgendas
                    .InsertAsync(() => new TheatreAgenda
                    {
                        ShowId = request.Agenda.ShowId,
                        DayId = request.Agenda.DayId,
                        TimeId = request.Agenda.TimeId,
                        AgendaDate = request.Agenda.AgendaDate,
                        TicketsQty = request.Agenda.TicketsQty
                    }, cancellationToken);

                var id = _db.TheatreAgendas.FirstOrDefault(ts => ts.ShowId == request.Agenda.ShowId && 
                                                                 ts.DayId == request.Agenda.DayId &&
                                                                 ts.TimeId == request.Agenda.TimeId)?.Id;

                return id ?? 0;
            }
        }
    }
}
