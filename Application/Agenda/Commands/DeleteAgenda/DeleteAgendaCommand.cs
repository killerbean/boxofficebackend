﻿using MediatR;
using LinqToDB;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;

namespace Application.Agenda.Commands.DeleteAgenda
{
    public class DeleteAgendaCommand : IRequest
    {
        public int Id { get; set; }
        
        public class DeleteShowCommandHandler : IRequestHandler<DeleteAgendaCommand>
        {
            private readonly IApplicationDbContext _db;
            
            public DeleteShowCommandHandler(IApplicationDbContext db)
            {
                _db = db;
            }
            
            public async Task<Unit> Handle(DeleteAgendaCommand request, CancellationToken cancellationToken)
            {
                var entity = await _db.TheatreAgendas
                    .FirstOrDefaultAsync(ts => ts.Id == request.Id, cancellationToken);

                if (entity == null)
                {
                    return Unit.Value;
                }

                await _db.TheatreAgendas
                    .Where(ts => ts.Id == request.Id)
                    .DeleteAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}
