﻿using MediatR;
using LinqToDB;
using AutoMapper;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using AutoMapper.QueryableExtensions;
using Application.Agenda.Queries.GetAgendas;

namespace Application.Agenda.Queries.GetAgenda
{
    public class GetAgendaQuery : IRequest<AgendaVm>
    {
        public long Id { get; set; }
        
        public class GetAgendaQueryHandler : IRequestHandler<GetAgendaQuery, AgendaVm>
        {
            private readonly IApplicationDbContext _db;
            private readonly IMapper _mapper;
            public GetAgendaQueryHandler(IApplicationDbContext db, IMapper mapper)
            {
                _db = db;
                _mapper = mapper;
            }
            
            public async Task<AgendaVm> Handle(GetAgendaQuery request, CancellationToken cancellationToken)
            {
                var vm = new AgendaVm
                {
                    Lists = await _db.TheatreAgendas
                        .ProjectTo<AgendaDto>(_mapper.ConfigurationProvider)
                        .Where(ts => ts.Id == request.Id)
                        .ToListAsync(cancellationToken)
                };

                return vm;
            }
        }
    }
}
