﻿using System;
using AutoMapper;
using Domain.Entities;
using Application.Common.Mappings;

namespace Application.Agenda.Queries.GetAgendas
{
    public class AgendaDto : IMapFrom<TheatreAgenda>
    {
        public int Id { get; set; }
        public int? ShowId { get; set; }
        public TheatreShow Show { get; set; }
        public int? DayId { get; set; }
        public DayOfTheWeek Day { get; set; }
        public int? TimeId { get; set; }
        public TimeOfTheDay Time { get; set; }
        public DateTime? AgendaDate { get; set; }
        public int? TicketsQty { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap(typeof(TheatreAgenda), GetType());
        }
    }
}
