﻿using MediatR;
using LinqToDB;
using AutoMapper;
using System.Linq;
using Domain.Common;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using AutoMapper.QueryableExtensions;

namespace Application.Agenda.Queries.GetAgendas
{
    public class GetAgendasQuery : IRequest<AgendaVm>
    {
        public PaginationFilter Filter { get; set; }

        public class GetAgendasQueryHandler : IRequestHandler<GetAgendasQuery, AgendaVm>
        {
            private readonly IApplicationDbContext _db;
            private readonly IMapper _mapper;
            public GetAgendasQueryHandler(IApplicationDbContext db, IMapper mapper)
            {
                _db = db;
                _mapper = mapper;
            }
            
            public async Task<AgendaVm> Handle(GetAgendasQuery request, CancellationToken cancellationToken)
            {
                var vm = new AgendaVm();
                if (request.Filter == null || request.Filter?.PageNumber == 0 && request.Filter?.PageSize == 0)
                {
                    vm.Lists = await _db.TheatreAgendas
                        .ProjectTo<AgendaDto>(_mapper.ConfigurationProvider)
                        .OrderBy(ta => ta.Id)
                        .ToListAsync(cancellationToken);                    
                }
                else
                {
                    var skip = (request.Filter.PageNumber - 1) * request.Filter.PageSize;

                    vm.Lists = await _db.TheatreAgendas
                        .ProjectTo<AgendaDto>(_mapper.ConfigurationProvider)
                        .Skip(skip < 0 ? 0 : skip)
                        .Take(request.Filter.PageSize)
                        .OrderBy(ta => ta.Id)
                        .ToListAsync(cancellationToken);
                }
                
                foreach (var agenda in vm.Lists)
                {
                    agenda.Show = await _db.TheatreShows
                        .FirstOrDefaultAsync(tsh => tsh.Id == agenda.ShowId, cancellationToken);                          
                    agenda.Day = await _db.DaysOfTheWeek
                        .FirstOrDefaultAsync(dow => dow.Id == agenda.DayId, cancellationToken);
                    agenda.Time = await _db.TimesOfTheDay
                        .FirstOrDefaultAsync(tod => tod.Id == agenda.TimeId, cancellationToken);
                }
                
                return vm;
            }
        }
    }
}
