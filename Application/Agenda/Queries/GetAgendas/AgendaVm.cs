﻿using System.Collections.Generic;

namespace Application.Agenda.Queries.GetAgendas
{
    public class AgendaVm
    {
        public IList<AgendaDto> Lists { get; set; }
    }
}
