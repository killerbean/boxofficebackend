﻿using System.Reflection;
using Application.Common;
using Application.Common.Interfaces;
using Application.Common.Services;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services
                .AddFluentEmail("cute.theatre@box.office")
                .AddRazorRenderer()
                //TODO: change this credentials for real one
                .AddSmtpSender("localhost", 25);

            services.AddScoped<ICurrentUserService, CurrentUserService>();

            return services;
        }
    }
}
