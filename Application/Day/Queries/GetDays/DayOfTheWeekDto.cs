﻿using AutoMapper;
using Domain.Entities;
using Application.Common.Mappings;

namespace Application.Day.Queries.GetDays
{
    public class DayOfTheWeekDto : IMapFrom<DayOfTheWeek>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public void Mapping(Profile profile)
        {
            profile.CreateMap(typeof(DayOfTheWeek), GetType());
        }
    }
}
