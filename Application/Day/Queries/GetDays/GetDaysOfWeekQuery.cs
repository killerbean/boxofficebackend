﻿using MediatR;
using LinqToDB;
using AutoMapper;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using AutoMapper.QueryableExtensions;

namespace Application.Day.Queries.GetDays
{
    public class GetDaysOfWeekQuery : IRequest<DayOfTheWeekVm>
    {
        public class GetAgendasQueryHandler : IRequestHandler<GetDaysOfWeekQuery, DayOfTheWeekVm>
        {
            private readonly IApplicationDbContext _db;
            private readonly IMapper _mapper;
            
            public GetAgendasQueryHandler(IApplicationDbContext db, IMapper mapper)
            {
                _db = db;
                _mapper = mapper;
            }
            
            public async Task<DayOfTheWeekVm> Handle(GetDaysOfWeekQuery request, CancellationToken cancellationToken)
            {
                var vm = new DayOfTheWeekVm
                {
                    Lists = await _db.DaysOfTheWeek
                        .ProjectTo<DayOfTheWeekDto>(_mapper.ConfigurationProvider)
                        .ToListAsync(cancellationToken)
                };

                return vm;
            }
        }
    }
}