﻿using System.Collections.Generic;

namespace Application.Day.Queries.GetDays
{
    public class DayOfTheWeekVm
    {
        public IList<DayOfTheWeekDto> Lists { get; set; }
    }
}
