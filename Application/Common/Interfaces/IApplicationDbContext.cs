﻿using Domain.Entities;
using LinqToDB;

namespace Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        ITable<DayOfTheWeek> DaysOfTheWeek { get; }
        ITable<TimeOfTheDay> TimesOfTheDay { get; }
        ITable<TheatreShow> TheatreShows { get; }
        ITable<User> Users { get; }
        ITable<TheatreAgenda> TheatreAgendas { get; }
        ITable<Order> Orders { get; }
    }
}
