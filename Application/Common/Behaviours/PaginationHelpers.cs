﻿using Domain.Common;

namespace Application.Common.Behaviours
{
    public class PaginationHelpers
    {
        public static PagedResponse<T> CreatePaginationResponse<T>(PaginationFilter filter, PagedResponse<T> response)
        {
            var nextPage = new PaginationQuery
            {
                PageNumber = filter?.PageNumber + 1
            };
            
            var prevPage = new PaginationQuery
            {
                PageNumber = filter?.PageNumber <= 1 ? null : filter?.PageNumber - 1
            };

            return new PagedResponse<T>(response.Lists)
            {
                NextPageNumber = nextPage.PageNumber, 
                PreviousPageNumber = prevPage.PageNumber,
                PageNumber = filter?.PageNumber > 0 ? filter?.PageNumber : null,
                PageSize = filter?.PageSize <= 0 ? 6 : filter?.PageSize
            };
        }
    }
}
