# BoxOfficeBackend

## Использованы следующие технологии:
1. .NetCore 3.1
2. AutoMapper - маппинг дтошек
3. FluentMigrator - миграция БД
4. FluentEmail - почта
5. MediatR - реализация паттерна mediator для .Net
6. Serilog - система логирования
7. LinqToDb - ORM вместо EntityFramework

Рабочая версия сайта. При первом запуске долго стартует контейнер, надо подождать 
https://boxofficef4ntec.z22.web.core.windows.net/

# Состояние проекта - авторизация
Готово все по ТЗ кроме авторизации. Как это бы я делал в 2х словах использую JWT авторизацию: 
## 
* Сначала в ConfigureServices добавляем авторизацию

```
var key = Encoding.ASCII.GetBytes(tokens.Key);
services.AddAuthentication(x =>
    {
        x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    })
    .AddJwtBearer(x =>
    {
        x.RequireHttpsMetadata = false;
        x.SaveToken = true;

        x.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(key),
            ValidateIssuer = false,
            ValidateAudience = false
        };
    });
```
## 
* Создаем эндпоинты register и login
register просто добавляет пользователя в БД
login сверяет пароль, который пришел с фронта в БД, потом генерирует обратный токен
```
// authentication successful so generate jwt token
var tokenHandler = new JwtSecurityTokenHandler();
var key = Encoding.ASCII.GetBytes(_tokens.Key);
var tokenDescriptor = new SecurityTokenDescriptor
{
    Subject = new ClaimsIdentity(new[]
    {
        new Claim(ClaimTypes.Name, user.Email),
        new Claim(ClaimTypes.Role, user.Role)
    }),
    Expires = DateTime.UtcNow.AddDays(2),
    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
};
var token = tokenHandler.CreateToken(tokenDescriptor);
user.Token = tokenHandler.WriteToken(token);
UpdateUserById(user.Id, user);

user.Password = null;
```
## 
* На фронте заглушка авторизации есть, там при регистрации шифруем пароль, чтобы не передавался в открытом виде
## 
* После успешной авторизации для проставления токена во все запросы пишем сервис-интерсептор
```
@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(
        private userService: UserService
    ) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        const currentUser = this.userService.getCurrentUser();

        if (currentUser && currentUser.token) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${currentUser.token}`
                }
            });
        }

        return next.handle(request);
    }
}
```
# Состояние проекта - дополнительно
1. Планировал реализовать список заказов, если пользователь авторизирован, то хранить его историю, даже соответствующие методы на беке и таблицу сдела, но не успел. Там ничего такого, чего уже не реализовано в проект. 
2. Также панировал сделать валидацию данных, которые передает пользователь. Примеры валидации есть в моем другом открытом проекте "git clone https://killerbean@bitbucket.org/killerbean/glycemic-catalog.git"

В целом все, есть вопросы - спрашивайте :-)
