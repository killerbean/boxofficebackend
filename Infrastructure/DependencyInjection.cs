﻿using System;
using Application.Common.Interfaces;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Initialization;
using LinqToDB.Data;
using Infrastructure.Persistence;
using Infrastructure.Persistence.Migrations;
using Infrastructure.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            LinqToDB.Common.Configuration.Linq.AllowMultipleQuery = true;
            DataConnection.DefaultSettings = new DatabaseSettings(configuration.GetConnectionString("DefaultConnection"));
            services.AddScoped<IApplicationDbContext, DB>();
            // services.AddScoped(s => new DB());
            services.AddTransient<IDateTime, DateTimeService>();
            
            services.AddFluentMigratorCore()
                .ConfigureRunner(rb => rb
                    .AddSqlServer()
                    .WithGlobalConnectionString(configuration.GetConnectionString("DefaultConnection"))
                    // Define the assembly containing the migrations
                    .ScanIn(typeof(Migration001IinitDB).Assembly).For.Migrations());
            
            return services;
        }

        public static IApplicationBuilder UseInfrastructure(this IApplicationBuilder builder, IMigrationRunner dbmigrator)
        {
            dbmigrator.MigrateUp();
            
            return builder;
        }
    }
}
