﻿using System.Collections.Generic;
using System.Linq;
using LinqToDB.Configuration;

namespace Infrastructure.Persistence
{
    public class DatabaseSettings : ILinqToDBSettings
    {
        private readonly string _connectionString;

        public DatabaseSettings(string connectionsString)
        {
            _connectionString = connectionsString;
        }
         
        public IEnumerable<IDataProviderSettings> DataProviders => Enumerable.Empty<IDataProviderSettings>();

        public string DefaultConfiguration => "SqlServer";
        public string DefaultDataProvider => "SqlServer";

        public IEnumerable<IConnectionStringSettings> ConnectionStrings
        {
            get
            {
                yield return
                    new ConnectionStringSettings
                    {
                        Name = "trader",
                        ProviderName = "SqlServer",
                        ConnectionString = _connectionString
                    };
            }
        }
    }

    public class ConnectionStringSettings : IConnectionStringSettings
    {
        public string ConnectionString { get; set; }
        public string Name { get; set; }
        public string ProviderName { get; set; }
        public bool IsGlobal => false;
    }
}
