﻿using FluentMigrator;
using FluentMigrator.SqlServer;
using Application.Common.Interfaces;
using Domain.Entities;
using Infrastructure.Persistence.DBHelpers;

namespace Infrastructure.Persistence.Migrations
{
    [Migration(01052020141200)]
    public class Migration001IinitDB : Migration
    {
        private readonly IDateTime _dateTime;

        public Migration001IinitDB(IDateTime dateTime)
        {
            _dateTime = dateTime;
        }

        public override void Up()
        {
            CreateDayOfWeekTable();
            CreateTimeOfTheDay();
            CreateShowTable();
            CreateUserTable();
            CreateAgendaTable();
            CreateOrderTable();
        }

        public override void Down()
        {

        }
        
        private void CreateDayOfWeekTable()
        {
            if (Schema.Table("TBL_F4NTEC_DAY_OF_WEEK").Exists())
            {
                return;
            }

            Create.Table("TBL_F4NTEC_DAY_OF_WEEK")
                .WithColumn(nameof(DayOfTheWeek.Id).ToDB()).AsInt64().NotNullable().PrimaryKey().Identity()
                .WithColumn(nameof(DayOfTheWeek.Name).ToDB()).AsString(256).NotNullable();

            Insert.IntoTable("TBL_F4NTEC_DAY_OF_WEEK")
                .WithIdentityInsert()
                .Row(new { Id = 1, Name = "Понедельник"})
                .Row(new { Id = 2, Name = "Вторник"})
                .Row(new { Id = 3, Name = "Среда"})
                .Row(new { Id = 4, Name = "Четверг"})
                .Row(new { Id = 5, Name = "Пятница"})
                .Row(new { Id = 6, Name = "Суббота"})
                .Row(new { Id = 7, Name = "Воскресенье"});
        }
        
        private void CreateTimeOfTheDay()
        {
            if (Schema.Table("TBL_F4NTEC_TIME_OF_THE_DAY").Exists())
            {
                return;
            }

            Create.Table("TBL_F4NTEC_TIME_OF_THE_DAY")
                .WithColumn(nameof(DayOfTheWeek.Id).ToDB()).AsInt64().NotNullable().PrimaryKey().Identity()
                .WithColumn(nameof(DayOfTheWeek.Name).ToDB()).AsString(256).NotNullable();

            Insert.IntoTable("TBL_F4NTEC_TIME_OF_THE_DAY")
                .WithIdentityInsert()
                .Row(new {Id = 1, Name = "12:00"})
                .Row(new {Id = 2, Name = "13:00"})
                .Row(new {Id = 3, Name = "14:00"})
                .Row(new {Id = 4, Name = "15:00"})
                .Row(new {Id = 5, Name = "16:00"})
                .Row(new {Id = 6, Name = "17:00"})
                .Row(new {Id = 7, Name = "18:00"})
                .Row(new {Id = 8, Name = "19:00"})
                .Row(new {Id = 9, Name = "20:00"})
                .Row(new {Id = 10, Name = "21:00"})
                .Row(new {Id = 11, Name = "22:00"})
                .Row(new {Id = 12, Name = "23:00"});
        }

        private void CreateShowTable()
        {
            if (Schema.Table("TBL_F4NTEC_SHOW").Exists())
            {
                return;
            }

            Create.Table("TBL_F4NTEC_SHOW")
                .WithColumn(nameof(TheatreShow.Id).ToDB()).AsInt64().NotNullable().PrimaryKey().Identity()
                .WithColumn(nameof(TheatreShow.Name).ToDB()).AsString(256).NotNullable()
                .WithColumn(nameof(TheatreShow.PosterUrl).ToDB()).AsString(2048).NotNullable()
                
                .WithColumn(nameof(TheatreShow.Created).ToDB()).AsDateTime().NotNullable()
                .WithColumn(nameof(TheatreShow.CreatedBy).ToDB()).AsString(256).NotNullable()
                .WithColumn(nameof(TheatreShow.LastModified).ToDB()).AsDateTime().Nullable()
                .WithColumn(nameof(TheatreShow.LastModifiedBy).ToDB()).AsString(256).Nullable();
        }

        private void CreateUserTable()
        {
            if (Schema.Table("TBL_F4NTEC_USER").Exists())
            {
                return;
            }

            Create.Table("TBL_F4NTEC_USER")
                .WithColumn(nameof(User.Id).ToDB()).AsInt64().NotNullable().PrimaryKey().Identity()
                .WithColumn(nameof(User.Email).ToDB()).AsString(512).NotNullable()
                .WithColumn(nameof(User.Password).ToDB()).AsString(512).NotNullable()
                .WithColumn(nameof(User.IsRegistered).ToDB()).AsBoolean().NotNullable()
                .WithColumn(nameof(User.IsAdmin).ToDB()).AsBoolean().NotNullable()
                
                .WithColumn(nameof(User.Created).ToDB()).AsDateTime().NotNullable()
                .WithColumn(nameof(User.CreatedBy).ToDB()).AsString(256).NotNullable()
                .WithColumn(nameof(User.LastModified).ToDB()).AsDateTime().Nullable()
                .WithColumn(nameof(User.LastModifiedBy).ToDB()).AsString(256).Nullable();

            Insert.IntoTable("TBL_F4NTEC_USER")
                .WithIdentityInsert()
                .Row(new
                {
                    Id = 1,
                    Email = "admin@admin.admin",
                    Password = "admin",
                    Is_Registered = true,
                    Is_Admin = true,
                    Created = _dateTime.Now,
                    Created_By = "admin@admin.admin"
                });
        }

        private void CreateAgendaTable()
        {
            if (Schema.Table("TBL_F4NTEC_AGENDA").Exists())
            {
                return;
            }

            Create.Table("TBL_F4NTEC_AGENDA")
                .WithColumn(nameof(TheatreAgenda.Id).ToDB()).AsInt64().NotNullable().PrimaryKey().Identity()
                .WithColumn(nameof(TheatreAgenda.ShowId).ToDB()).AsInt64().Nullable()
                .WithColumn(nameof(TheatreAgenda.DayId).ToDB()).AsInt64().Nullable()
                .WithColumn(nameof(TheatreAgenda.TimeId).ToDB()).AsInt64().Nullable()
                .WithColumn(nameof(TheatreAgenda.AgendaDate).ToDB()).AsDateTime().Nullable()
                .WithColumn(nameof(TheatreAgenda.TicketsQty).ToDB()).AsInt64().Nullable();

            Create.ForeignKey()
                .FromTable("TBL_F4NTEC_AGENDA").ForeignColumn("show_id")
                .ToTable("TBL_F4NTEC_SHOW").PrimaryColumn("id");
            
            Create.ForeignKey()
                .FromTable("TBL_F4NTEC_AGENDA").ForeignColumn("day_id")
                .ToTable("TBL_F4NTEC_DAY_OF_WEEK").PrimaryColumn("id");     
            
            Create.ForeignKey()
                .FromTable("TBL_F4NTEC_AGENDA").ForeignColumn("time_id")
                .ToTable("TBL_F4NTEC_TIME_OF_THE_DAY").PrimaryColumn("id");  
        }

        private void CreateOrderTable()
        {
            if (Schema.Table("TBL_F4NTEC_ORDER").Exists())
            {
                return;
            }

            Create.Table("TBL_F4NTEC_ORDER")
                .WithColumn(nameof(Order.Id).ToDB()).AsInt64().NotNullable().PrimaryKey().Identity()
                .WithColumn(nameof(Order.UserId).ToDB()).AsInt64().Nullable()
                .WithColumn(nameof(Order.AgendaId).ToDB()).AsInt64().Nullable()
                .WithColumn(nameof(Order.TicketsOrdered).ToDB()).AsInt64().Nullable();
            
            Create.ForeignKey()
                .FromTable("TBL_F4NTEC_ORDER").ForeignColumn("user_id")
                .ToTable("TBL_F4NTEC_USER").PrimaryColumn("id");  
            
            Create.ForeignKey()
                .FromTable("TBL_F4NTEC_ORDER").ForeignColumn("agenda_id")
                .ToTable("TBL_F4NTEC_AGENDA").PrimaryColumn("id");  
        }
    }
}
