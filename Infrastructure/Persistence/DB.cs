﻿using System;
using Application.Common.Interfaces;
using Domain.Entities;
using Serilog;
using LinqToDB;
using LinqToDB.Data;

namespace Infrastructure.Persistence
{
    public class DB : DataConnection, IApplicationDbContext
    {
        public ITable<DayOfTheWeek> DaysOfTheWeek { get; set; }
        public ITable<TimeOfTheDay> TimesOfTheDay { get; set; }
        public ITable<TheatreShow> TheatreShows { get; set; }
        public ITable<User> Users { get; set; }
        public ITable<TheatreAgenda> TheatreAgendas { get; set; }
        public ITable<Order> Orders { get; set; }

        public DB()
        {
            InitDBContext();
            InitTables();
        }
        
        private void InitDBContext()
        {
            CommandTimeout = 180;
            TurnTraceSwitchOn();
            OnTraceConnection = e =>
            {
                if (e.TraceInfoStep == TraceInfoStep.AfterExecute)
                    Log.Debug("Step: {@TraceInfoStep}, Time: {@ExecutionTime}, Query: {@SqlText}", 
                        e.TraceInfoStep, e.ExecutionTime, e.SqlText.Substring(0, 
                            Math.Min(32768, e.SqlText.Length)));
            };
        }

        private void InitTables()
        {
            DaysOfTheWeek = GetTable<DayOfTheWeek>();
            TimesOfTheDay = GetTable<TimeOfTheDay>();
            TheatreShows = GetTable<TheatreShow>();
            Users = GetTable<User>();
            TheatreAgendas = GetTable<TheatreAgenda>();
            Orders = GetTable<Order>();
        }
    }
}
